class DataModel {
	int? count;
	List<String>? categories;

	DataModel({this.count, this.categories});

	factory DataModel.fromJson(Map<String, dynamic> json) => DataModel(
				count: json['count'] as int?,
				categories: json['categories'] as List<String>?,
			);

	Map<String, dynamic> toJson() => {
				'count': count,
				'categories': categories,
			};
}
