import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'data_model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Filter Table - test code',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Filter Taable - test code'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ScrollController? controller;
  TextEditingController inputFilter = TextEditingController(text: '');
  List<dynamic>? category;
  List<dynamic>? filteredCategory;
  int categorieCount = 0;
  Future<dynamic> fetchCategories() async {
    try {
      final res =
          await http.get(Uri.parse('https://api.publicapis.org/categories'));
      return res.body;
    } catch (e) {
      return e;
    }
  }

  @override
  void initState() {
    super.initState();
    loadCategories();
  }

  loadCategories() async {
    final data = await fetchCategories();
    Map<String, dynamic>? jsonData = jsonDecode(data);

    setState(() {
      category = jsonData!['categories'];

      categorieCount = jsonData['count'];
      filteredCategory = category;
    });
  }

  onFilter() {
    var inputFilterLower = inputFilter.text.toString().toLowerCase();
    filteredCategory = category!
        .where((element) 
        { var elementLower = element.toString().toLowerCase();
          
          return elementLower.startsWith(inputFilterLower)?true:false;
        } )
        .toList();
    setState(() {
      categorieCount = filteredCategory!.length;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    List<DataRow> dataRows = [];
    for (var i = 0; i < categorieCount; i++) {
      dataRows.add(DataRow(cells: <DataCell>[
        DataCell(
          SizedBox(
            width: double.infinity,
            child: Text(
              filteredCategory![i],
              textAlign: TextAlign.center,
            ),
          ),
        )
      ]));
    }
    return Scaffold(
        body: SingleChildScrollView(
      child: Center(
          child: Column(children: <Widget>[
        Container(
          width: width * 0.3,
          margin: EdgeInsets.all(20),
          child: TextField(
            onChanged: (value) => onFilter(),
            controller: inputFilter,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'filter',
            ),
          ),
        ),
        DataTable(
          headingRowColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered))
              return Theme.of(context).colorScheme.primary.withOpacity(0.08);
            return Colors.blue.shade200; // Use the default value.
          }),
          showCheckboxColumn: false,
          columns: const <DataColumn>[
            DataColumn(
              label: Center(
                child: Text(
                  'Categories',
                  style: TextStyle(fontSize: 33, fontStyle: FontStyle.normal),
                ),
              ),
            ),
          ],
          rows: dataRows,
        ),
      ])),
    ));
  }
}
